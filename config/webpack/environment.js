const { environment } = require('@rails/webpacker')
const coffee =  require('./loaders/coffee')
const jquery = require('./plugins/jquery')

environment.plugins.prepend('jquery', jquery)
environment.loaders.prepend('coffee', coffee)
module.exports = environment
