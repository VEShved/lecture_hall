Rails.application.routes.draw do
  root 'courses#index'
  resources :courses, only: :index do
    resources :groups, only: :index
  end

  resources :groups, only: :show do
    resources :registrations, only: :create
  end
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
