class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses do |t|
      t.string :name, null: false
      t.datetime :deleted_at, index: true

      t.timestamps

      t.index :name, unique: true
    end
  end
end
