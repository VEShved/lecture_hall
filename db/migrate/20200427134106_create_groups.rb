class CreateGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :groups do |t|
      t.datetime :starts_at, null: false
      t.belongs_to :course, null: false, foreign_key: true
      t.datetime :deleted_at

      t.timestamps
    end
  end
end
