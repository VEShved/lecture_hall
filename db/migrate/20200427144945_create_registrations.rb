class CreateRegistrations < ActiveRecord::Migration[6.0]
  def change
    create_table :registrations do |t|
      t.string :email
      t.belongs_to :group, null: false, foreign_key: true

      t.timestamps
    end
  end
end
