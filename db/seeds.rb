if Rails.env.development?
  AdminUser.create!(email: 'admin@example.com',
                    password: 'password', password_confirmation: 'password')
  course_names = ['Веб-разработка', 'Python-разработка', 'Продакт-менеджмент',
                  'Интернет-маркетинг', 'SMM-менеджмент', 'Дизайн',
                  'Дизайн интерьера', 'Android-разработка']
  Course.create(course_names.map { |name| { name: name } })

  Course.offset(1).each do |course|
    3.times do |offset|
      group = course.groups.create!(starts_at: 15.hours.ago + offset.days)
      group.registrations.create!(
        Array.new(10) { |i| { email: "student#{i}@example.com" } }
      )
    end
  end
end
