require 'rails_helper'

RSpec.describe 'Registrations', type: :request do
  describe 'POST /groups/:group_id/registrations' do
    subject(:make_request) do
      post "/groups/#{group_id}/registrations", params: params
      response
    end

    let(:params) { { registration: { email: email } } }
    let(:email) { 'some@email.com' }
    let(:group_id) { group.id }
    let(:group) { create(:group) }

    context 'when group_id is wrong' do
      let(:group_id) { -1 }

      it { is_expected.to be_not_found }

      it 'responds group error' do
        make_request
        expect(response_body).to eq(errors: ['Not found'])
      end
    end

    context 'when wrong email was passed' do
      let(:email) { 'wrong email' }

      it { is_expected.to be_unprocessable }

      it 'responds error' do
        make_request
        expect(response_body).to eq(errors: ['Email is invalid'])
      end
    end

    context 'when params are valid' do
      it { is_expected.to be_created }

      it "doesn't response error" do
        make_request
        expect(response.body).to be_blank
      end
    end
  end
end
