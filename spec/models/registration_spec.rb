require 'rails_helper'

RSpec.describe Registration, type: :model do
  it { is_expected.to allow_value('some_email@addresse.com').for(:email) }
  it { is_expected.not_to allow_value('foo').for(:email) }
  it { is_expected.not_to allow_value('').for(:email) }
end
