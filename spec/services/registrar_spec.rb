require 'rails_helper'

RSpec.describe Registrar, type: :service do
  describe '#call' do
    subject(:register) { registrar.call }

    let(:registrar) { described_class.new(email: email, group: group) }
    let(:email) { 'some@email.com' }
    let(:group) { create(:group) }

    context 'when no group was passed' do
      let(:group) { nil }

      it { is_expected.to eq(errors: ['Group must exist']) }
    end

    context 'when wrong email was passed' do
      let(:email) { 'wrong email' }

      it { is_expected.to eq(errors: ['Email is invalid']) }
    end

    context 'when params are valid' do
      it { is_expected.to eq(result: true) }
    end
  end
end
