FactoryBot.define do
  factory :registration do
    group
    sequence(:email) { |i| "some_email#{i}@domain.rf" }
  end
end
