FactoryBot.define do
  factory :group do
    course
    starts_at { 1.day.from_now }
  end
end
