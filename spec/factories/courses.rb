FactoryBot.define do
  factory :course do
    sequence(:name) { |i| "course Name #{i}" }
  end
end
