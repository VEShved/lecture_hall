class Registration < ApplicationRecord
  belongs_to :group

  validates :email, format: { with: /\A[^@]+@[^\.]+\..+\z/ }
end
