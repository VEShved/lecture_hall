class Group < ApplicationRecord
  acts_as_paranoid

  belongs_to :course

  has_many :registrations, dependent: :destroy

  scope :after_now, -> { where('starts_at > ?', Time.current) }

  def in_the_future?
    starts_at > Time.current
  end
end
