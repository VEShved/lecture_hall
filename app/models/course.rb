class Course < ApplicationRecord
  acts_as_paranoid

  validates :name, presence: true
  validates :name, uniqueness: true

  has_many :groups, dependent: :destroy

  accepts_nested_attributes_for :groups, allow_destroy: true

  scope :sorted_by_starts_at,
        lambda {
          groups = Group.arel_table
          groups_join = arel_table.join(groups, Arel::Nodes::OuterJoin)
                                  .on(
                                    arel_table[:id]
                                      .eq(groups[:course_id])
                                      .and(groups[:starts_at].gt(Time.current))
                                  ).join_sources
          joins(groups_join).group(:id).order('MIN(groups.starts_at)')
        }
end
