class CoursesController < ApplicationController
  def index
    @courses = Course.includes(groups: :registrations)
    if params[:order_by] == 'starts_at'
      @courses = @courses.sorted_by_starts_at
      @ordered = true
    end
    @courses = @courses.all.decorate
  end
end
