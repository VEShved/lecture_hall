class RegistrationsController < ApplicationController
  def create
    if registration_result[:errors].blank?
      head :created
    else
      render json: { errors: registration_result[:errors] },
             status: :unprocessable_entity
    end
  end

  private

  def registration_result
    @registration_result ||=
      Registrar.new(email: params.require(:registration)[:email],
                    group: Group.after_now.find(params[:group_id])).call
  end
end
