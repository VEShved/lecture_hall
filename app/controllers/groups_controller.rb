class GroupsController < ApplicationController
  def index
    @groups = Group.after_now
                   .includes(:registrations)
                   .where(course_id: params[:course_id]).decorate
  end

  def show
    @group = Group.find(params[:id]).decorate
    @course = @group.course.decorate
    @registrations = @group.registrations.decorate
  end
end
