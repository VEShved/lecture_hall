ActiveAdmin.register Course do
  permit_params :name, groups_attributes: %i[starts_at]

  index do
    column :name
    actions
  end

  form do |f|
    f.inputs do
      f.input :name
    end

    f.has_many :groups, allow_destroy: true do |g|
      g.inputs do
        g.input :starts_at
      end
    end

    actions
  end

  show do
    attributes_table do
      row :name
    end

    panel 'Groups' do
      table_for course.groups do
        column :starts_at
        column :registrations_count do |group|
          group.registrations.count
        end
      end
    end
  end
end
