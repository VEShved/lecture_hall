class Registrar
  def initialize(email:, group:)
    @email = email
    @group = group
  end

  def call
    if registration.valid?
      registration.save!
      { result: true }
    else
      { errors: registration.errors.full_messages }
    end
  end

  private

  attr_reader :email, :group

  def registration
    @registration ||= Registration.new(
      email: email,
      group: group
    )
  end
end
