$ ->
  $(document).on(
    'ajax:success'
    'form.registration_form'
    -> window.location.reload()
  ).on(
    'ajax:error'
    'form.registration_form'
    (e) ->
      errors = e.detail[0].errors
      $(e.currentTarget).find('.error_message').text(errors).show()
  )
