class CourseDecorator < Draper::Decorator
  delegate_all

  def starts_at
    groups.map { |group| group.starts_at if group.in_the_future? }.compact.min
  end

  def registrations_count
    groups.map do |group|
      group.registrations if group.in_the_future?
    end.compact.flatten.count
  end
end
