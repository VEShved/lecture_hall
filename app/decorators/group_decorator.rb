class GroupDecorator < ApplicationDecorator
  delegate_all

  def registrations_count
    registrations.to_a.count
  end
end
